#include <sourcemod>
#include <updater>

#define UPDATE_URL "http://bitbucket.org/gavvvr/sm-hl2dm-spectate-delay/raw/master/specdelay.txt"

new Handle:specTimers[MAXPLAYERS+1];
new Handle:hudSynchronizer;

public Plugin:myinfo =
{
	name = "Spectate delay",
	author = "gavvvr",
	description = "Prevents instant switch to spectators team",
	version = "0.1.1",
	url = "https://hl2dm.net"
};

public OnPluginStart()
{
	if (LibraryExists("updater"))
	{
		Updater_AddPlugin(UPDATE_URL)
	}

	hudSynchronizer = CreateHudSynchronizer();
	AddCommandListener(Cmd_Jointeam, "jointeam");
	AddCommandListener(Cmd_Spectate, "spectate");
}

public Action:Cmd_Jointeam(client, const String:command[], args)
{
	if (GetClientTeam(client) != 1 && !IsFakeClient(client))
	{
		new String:jointeamArg[2];
		GetCmdArgString(jointeamArg, sizeof(jointeamArg));
		if (StrEqual(jointeamArg, "1"))
		{
			if (specTimers[client] == INVALID_HANDLE)
				{
					EchoOnSpecMsg(client);
					specTimers[client] = CreateTimer(3.0, PutInSpecs, client, TIMER_FLAG_NO_MAPCHANGE);
				}
			return Plugin_Handled;
		}
		else if (specTimers[client] != INVALID_HANDLE)
		{
			EchoOnJoinMsg(client);
			KillTimer(specTimers[client]);
			specTimers[client] = INVALID_HANDLE;
		}
	}
	return Plugin_Continue;
}

public Action:Cmd_Spectate(client, const String:command[], args)
{
	if (!IsFakeClient(client) && GetClientTeam(client) != 1 && specTimers[client] == INVALID_HANDLE)
		{
			EchoOnSpecMsg(client);
			specTimers[client] = CreateTimer(3.0, PutInSpecs, client, TIMER_FLAG_NO_MAPCHANGE);
			return Plugin_Handled;
		}
	return Plugin_Continue;
}

EchoOnSpecMsg(client)
{
	SetHudTextParams(0.01, 0.1, 3.0, 20, 200, 0, 200, 1, 0.5, 0.1, 0.2);
	ShowSyncHudText(client, hudSynchronizer, "Spectate in 3 seconds");
}

EchoOnJoinMsg(client)
{
	SetHudTextParams(0.01, 0.1, 1.0, 20, 200, 0, 200, 1, 0.5, 0.1, 0.2);
	ShowSyncHudText(client, hudSynchronizer, "Cancelled");
}

public Action:PutInSpecs(Handle:timer, any:client)
{
	ChangeClientTeam(client, 1);
	specTimers[client] = INVALID_HANDLE;
}

public OnLibraryAdded(const String:name[])
{
	if (StrEqual(name, "updater"))
	{
		Updater_AddPlugin(UPDATE_URL)
	}
}
